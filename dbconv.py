#!/usr/bin/env python
# coding: utf-8

#vers
inputfile="gmail.txt" #important
outputfile="db.csv"
nu=1 #start number. important

#imports
import sys,re,time
import codecs
import getopt

regp="(.*):(.*):(.*):(.*)\r?\n?$"	#important

try:
	#opts, args = getopt.getopt(sys.argv[1:], 'u:h', ['user='])	#短选项 u带参 h不带 长选项user带参
	opts, args = getopt.getopt(sys.argv[1:], 'i:o:', ['help'])
except getopt.GetoptError:
	#watch for undefined parameters
	prinred ('Option not recognised. Type -h for help.\n')
	sys.exit(2)

for opt, arg in opts:
	if opt in ('-h', '--help'):
		print 'This is the SA help page. Writen by bearqq.\nSome useful argvs:\n'
		print '-i\tfor --input; defines input file\n'
		print '-o\tfor --output; defines output file\n'
		sys.exit()
	elif opt in ('-i', '--input'):
		inputfile=arg
	elif opt in ('-o', '--output'):
		outputfile=arg

fin=codecs.open(inputfile,'r','utf-8')
#fout=codecs.open(outputfile,'a','utf-8')
ferr=codecs.open("error.txt",'a','utf-8')

ni=1
notend=1
fout=codecs.open(str(nu)+outputfile,'a','utf-8')#
while notend:
	line=fin.readline()
	if line:#!='':
		rx1=re.search(regp,line,re.I)
		if rx1:
			fout.write("\"%s\",\"%s\",\"%s\",\"gmail\"\n" % (rx1.group(1),rx1.group(2),rx1.group(4))) #important for format and db name!
		else:
			ferr.write("%s" % line)
		ni=ni+1
		#begin
		if ni>60000:
			fout.close()
			nu=nu+1
			fout=codecs.open(str(nu)+outputfile,'a','utf-8')
			ni=1
	else:
		notend=0

fin.close()
fout.close()
ferr.close()
print "Finished!"
